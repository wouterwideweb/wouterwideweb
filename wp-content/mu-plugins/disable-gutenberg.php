<?php
/*
Plugin Name: Disable Gutenberg
*/

// Add to existing function.php file
function disable_gutenberg() {
  add_filter('use_block_editor_for_post', '__return_false');
}
add_action('admin_init', 'disable_gutenberg');
